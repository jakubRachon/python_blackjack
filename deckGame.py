import random
from sys import exit


class DeckOfCards():

    def __init__(self):
        self.colors = ['s','d','c','h']
        self.figure = ['2','3','4','5','6','7','8','9','10','J','Q','K','A']
        self.cards = []
        for i in self.colors:
            for j in self.figure:
                self.cards.append(j+i)
        #self.number_of_cards=len(self.cards)

    def show_cards(self):
        for card in self.cards:
            print card,
    def deal_card(self):
        poped_card=self.cards[len(self.cards)-1]
        self.cards.pop()
        return poped_card

    def shuffle_cards(self):
        random.shuffle(self.cards)

class Hand():
    def __init__(self):
        self.content = []
        self.points = 0

    def take_card(self,card):
        self.content.append(card)
        self.calculate_points()
        if self.points>21:
            print "Busted"
            self.lost_game()
            return False
        elif self.points:
            return True
        #self.show_hand()
        #print self.points

    def show_hand(self):
        for i in self.content:
            print i,
    def calculate_points(self):
        #print "content=%r" %self.content
            value = self.content[len(self.content)-1][:-1]
            if value=='J' or value=='Q' or value=='K':
                self.points += 10
            elif value=='A':
                if self.points>10:
                    self.points += 1
                else:
                    self.points += 11
            else:
                self.points += int(value)
    def lost_game(self):
        self.points=0
        self.content=[]



moja_talia = DeckOfCards()
reka_krupiera = Hand()
reka_gracza = Hand()

try:
    moja_talia.shuffle_cards()
    while True:
            moja_talia.show_cards()
            print "\n-------------------------\n"
            if reka_krupiera.points<17:
                reka_krupiera.take_card(moja_talia.deal_card())
            print "-------------------------\n"
            reka_krupiera.show_hand()
            print ", %d points \n" %(reka_krupiera.points)
            print "-------------------------\n"
            reka_gracza.show_hand()
            print ", %d points \n" %(reka_gracza.points)
            hit_or_stay=raw_input("Hit or Stay? (h/s)")
            if hit_or_stay=='h':
                if not reka_gracza.take_card(moja_talia.deal_card()):
                    reka_krupiera.lost_game()
            print "-------------------------\n"


            # print "ANSWER:  %s\n\n" % answer
except EOFError:
    print "\nBye"

# for i in range(0,3):
#     moja_talia.shuffle_cards()
#     moja_talia.show_cards()
#     print "\n"
#     print "-------------------------\n"
#     reka_krupiera.take_card(moja_talia.deal_card())
#     print "-------------------------\n"
#     reka_gracza.take_card(moja_talia.deal_card())
#
#
#
#     print "-------------------------\n"
